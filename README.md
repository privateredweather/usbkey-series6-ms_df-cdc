# README #

### What is this repository for? ###

This is a demo firmware implementation that merges the Mass Storage Demo implementation and the CDC Demo implementation from Atmel for the AT90USBKEY (AT90USB1287).

I created this because I wanted a way to send useful plain text debugging information from the AT90USBKEY over a simple serial connection while working on modifications to the Mass Storage implementation.  Perhaps there is another way I could easily have gotten information (without the use of JTAG or some other firmware development tools) but I couldn't think of any simpler way.

I used the series6 code examples to ensure that the two demos were as similar as possible before attempting to combine the functionality.  A newer series7 implementation for Mass Storage exists, but a series7 of the CDC implementation could not be found.

While attempting to perform this merger of two overlapping code bases, I discovered that the CDC (UART/COM) device will not be automatically recognized by OS X unless the CDC COMM interface is interface 0 and the CDC DATA interface is interface 1.  The Mass Storage Interface works regardless which interface number it is assigned in the USB Descriptor.  Presumably this is due to the nature of the CDC driver built into OS X, but perhaps there is some other explanation that I have overlooked.

The first commit is the unmodified Mass Storage code (without CDC functionality.)  The second commit includes the basic modifications to compile the Mass Storage demo on OS X.

### How do I get set up? ###

Roughly speaking, here are the things that you need to do:
####1) brew tap osx-cross/avr
####2) brew install avr-libc
####3) git clone/checkout the code repository
####4) cd to the gcc directory
####5) make
####6) plug in the AT90USBKEY and put it in DFU (firmware flashing) mode by holding the HWB button and tapping the RST button
####7) run ./flash

You may have to troubleshoot some specifics for your environment and configuration, especially if you are running an older or newer OS, version of avr-gcc, etc.

The ./flash script will automatically reset the AT90USBKEY and start in non-DFU mode.  If the onboard solid-state storage is holding a file system that can be recognized by OS X, it should automatically mount.  For me, it is a FAT16 volume called "NO NAME" that contains the data that shipped with the AT90USBKEY from the factory - libraries, starter kits, demos, documentation, etc.

You can also test the CDC implementation by running "ls /dev/tty* | less" and checking for a new tty device.  In my case, it was called /dev/tty.usbmodem1421.  It should appear when the AT90USBKEY has loaded from the firmware (non-DFU mode) and disappear if you unplug the AT90USBKEY or load it in DFU mode.  To test the CDC functionality, connect to the tty device, for example, by typing "screen /dev/tty.usbmodem1421".  If you move or press the joystick or if you press the HWB button, you should see notifications dumped to the terminal.